from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import SignupForm, OrderForm
from django.db.models import Max
from django.http import JsonResponse
from .tasks import start, scrape, point_five_hr_sleep,emergent_sleep
from items.models import Order, User_Email, ip_address, main_count_down, worker1_count_down
from django.http import HttpResponse
from django.http import FileResponse
import json
from api.create_pdf import create_pdf,create_blur_pdf, create_invoice
from api.send_notification_email import generate_verification_code, send_notification_email, find_file_with_any_extension
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth.models import User
import os
import datetime
from django.core.files.storage import FileSystemStorage
import requests
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import paramiko



contact_us = ['CONTACT', 'contact', 'Contact', "FIND", 'Find', 'find', '聯絡', '聯絡我們', '聯系', '聯系我們', '聯繫', '聯繫我們', '電話', '電郵']
about_us = ['ABOUT', 'about', 'About', '關於', 'Service', 'service', "SERVICE", '服務', 'Vision', 'vision', 'VISION', 'Mission', 'MISSION', 'mission', '使命', '簡介', '介紹', '背景', '公司簡要',
            'background', 'BACKGROUND', 'Background', 'Backgrounds', 'backgrounds']


# Create your views here.

def test(request):
    target_order = Order.objects.get(order_id=42)
    print(target_order.chin_pitch)
    testing_pitch = 'hahaha' + "\n" + "\n" + target_order.chin_pitch
    print(testing_pitch)
    #send_notification_email('matthewkwok181200@gmail.com', testing_pitch, 'adsf', 'adsf' )


def index(request):
    main_ending_time = None
    worker1_ending_time = None
    if main_count_down.objects.all().exists():
        count_down_main = list(main_count_down.objects.all())
        if timezone.now() < count_down_main[-1].main_end_date_time:
            main_ending_time = count_down_main[-1].main_end_date_time
            print(main_ending_time)
        elif timezone.now() >= count_down_main[-1].main_end_date_time:
            main_count_down.objects.all().delete()
            main_ending_time = None
    if worker1_count_down.objects.all().exists():
        count_down_worker1 = list(worker1_count_down.objects.all())
        if timezone.now() < count_down_worker1[-1].worker1_end_date_time:
            worker1_ending_time = count_down_worker1[-1].worker1_end_date_time
            print(worker1_ending_time)
        elif timezone.now() >= count_down_worker1[-1].worker1_end_date_time:
            worker1_count_down.objects.all().delete()
            worker1_ending_time = None

    print('main_ending_time', main_ending_time)
    print('worker1_ending_time', worker1_ending_time)

    if request.user.is_authenticated:
        username = request.user.username
        if request.method == 'POST':
            if 'stop_main' in request.POST:
                emergent_sleep.delay()
                count_down_main = main_count_down()
                count_down_main.main_end_date_time = timezone.now() + timedelta(hours=2)
                count_down_main.save()
                print('stopped main ip')
                return render(request, "index-8.html", {'username': username, 'main_ending_time': count_down_main.main_end_date_time, 'worker1_ending_time': worker1_ending_time})
            elif 'stop_worker1' in request.POST:
                print('stopped worker1 ip')
                requests.post('http://128.199.161.124:8000/emergent_sleep')
                count_down_worker1 = worker1_count_down()
                count_down_worker1.worker1_end_date_time = timezone.now() + timedelta(hours=2)
                count_down_worker1.save()
                return render(request, "index-8.html", {'username': username, 'main_ending_time': main_ending_time, 'worker1_ending_time': count_down_worker1.worker1_end_date_time})
            else:
                send_notification_email('analyticx01@gmail.com', f'{request.POST.get("comments")}, {request.POST.get("email")}', f'{request.POST.get("subject")}', 'enquire sent to admin')
                print('hi')
                return render(request, "index-8.html", {'username': username, 'main_ending_time': main_ending_time, 'worker1_ending_time': worker1_ending_time})

        else:
            return render(request, "index-8.html", {'username': username, 'main_ending_time': main_ending_time, 'worker1_ending_time': worker1_ending_time})

    else:
        if request.method == 'POST':
            send_notification_email('analyticx01@gmail.com',
                                    f'{request.POST.get("comments")}, {request.POST.get("email")}',
                                    f'{request.POST.get("subject")}', 'enquire sent to admin')

            return render(request, "index-8.html", {'main_ending_time': main_ending_time, 'worker1_ending_time': worker1_ending_time})
        else:
            return render(request, "index-8.html", {'main_ending_time': main_ending_time,  'worker1_ending_time': worker1_ending_time})


def privacy(request):
    return render(request, "privacy.html")

def tnc(request):
    return render(request, "tnc.html")

def all_orders(request):
    all_orders = list(Order.objects.all().order_by('-order_id').values())
    all_email_user_objects = list(User_Email.objects.all().values())
    new_all_orders = []

    for order in all_orders:
        for email_user_object in all_email_user_objects:
            if order['order_username'] == email_user_object['user_name']:
                order['user_email'] = email_user_object['user_email']
                order['user_email_password'] = email_user_object['user_email_password']
                order['action_for_email'] = email_user_object['action_for_email']
                order['email_start_date'] = email_user_object['email_start_date']
                order['email_end_date'] = email_user_object['email_end_date']
                order['login_email_password'] = email_user_object['login_email_password']
        new_all_orders.append(order)

    return render(request, "all_orders.html", {'all_orders': new_all_orders})


def scrapping(request):
    return render(request, "scrapping.html")

def generating_report(request):
    return render(request, "generating_report.html")

def my_orders(request):
    current_user = request.user.username
    user_orders = Order.objects.filter(order_user__username=current_user).order_by('-order_id')
    return render(request, "my_orders.html", {'my_orders': user_orders})



def signup_user(request):
    total_usernames = User.objects.all().values_list('username', flat=True)
    total_emails = User.objects.all().values_list('email', flat=True)

    if request.method == 'POST':
        form = SignupForm(request.POST)
        if 'get_verification' in request.POST:
            email = request.POST.get('email')
            if email:
                verification_code = generate_verification_code()
                try:
                    send_notification_email(email, f'你的驗證碼是:{verification_code}', 'Email 驗證 - AnalyticX',
                                            'verification email sent')
                    request.session['verification_code'] = verification_code
                    request.session['verification_code_expires'] = (timezone.now() + timedelta(seconds=60)).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    return JsonResponse({"status": "success"}, status=200)
                except:
                    error_message = "電郵地址不存在. 請重新輸入"
                    return JsonResponse({"status": "error", "message": error_message}, status=400)


                return render(request, 'signup.html', {'form': form})
            else:
                return render(request, 'signup.html',
                              {'error_message': "請輸入電郵地址", 'form': form})

        else:

            if request.POST.get('verification'):
                try:
                    try_get_expire = request.session['verification_code_expires']
                    session_expire = True
                except:
                    session_expire = False

                if session_expire:
                    verification_code_expires = request.session['verification_code_expires']
                    now = timezone.now()
                    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")
                    if formatted_now < verification_code_expires:
                        if request.POST.get('email') != '' and request.POST.get('verification') != '' and request.POST.get(
                                'username') != '' and request.POST.get('password1') != '' and request.POST.get(
                                'password2') != '':
                            username = request.POST.get('username')
                            email = request.POST.get('email')
                            password1 = request.POST.get('password1')
                            password2 = request.POST.get('password2')
                            entered_verification_code = request.POST.get('verification')
                            stored_verification_code = request.session.get('verification_code', None)

                            if stored_verification_code and entered_verification_code == stored_verification_code:
                                if username in total_usernames:
                                    error_message = "用戶名已存在，請重新輸入。請再使用現在的驗證碼注冊。"
                                    return JsonResponse({"status": "error", "message": error_message}, status=400)

                                elif email in total_emails:
                                    error_message = "電郵地址已存在，請重新輸入。請獲取新的驗證碼。"
                                    return JsonResponse({"status": "error", "message": error_message}, status=400)
                                elif password1 != password2:
                                    error_message = "密碼不一致。請再使用現在的驗證碼注冊。"
                                    return JsonResponse({"status": "error", "message": error_message}, status=400)
                                elif password1.isdigit():
                                    error_message = "密碼必需要有數字和英文字母。請再使用現在的驗證碼注冊。"
                                    return JsonResponse({"status": "error", "message": error_message}, status=400)

                                elif len(password1) < 8:
                                    error_message = "密碼至少要8位長。請再使用現在的驗證碼注冊。"
                                    return JsonResponse({"status": "error", "message": error_message}, status=400)

                                elif form.is_valid():
                                    user = form.save(commit=False)
                                    user.set_password(password1)
                                    user.save()

                                    # Log in the user
                                    login(request, user)

                            else:
                                error_message = "驗證碼錯誤，請重新輸入你剛獲得的的驗證碼。"
                                return JsonResponse({"status": "error", "message": error_message}, status=400)

                        else:
                            error_message = "請填寫全部資料。"
                            return JsonResponse({"status": "error", "message": error_message}, status=400)

                    else:
                        error_message = "驗證碼已過期，請獲取新的驗證碼。"
                        return JsonResponse({"status": "error", "message": error_message}, status=400)

                else:
                    error_message = "請先獲取驗證碼"
                    return JsonResponse({"status": "error", "message": error_message}, status=400)



            if not request.POST.get('verification'):
                error_message = "請輸入驗證碼"
                return JsonResponse({"status": "error", "message": error_message}, status=400)



            return render(request, 'signup.html', {'form': form})


    else:
        form = SignupForm()
    return render(request, 'signup.html', {'form': form})


def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            error_message = '登入失敗，請檢查你的用戶名和密碼是否正確。'
            return render(request, 'login.html', {'error_message': error_message})
    else:
        return render(request, 'login.html')

def password_forgot(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        new_password = request.POST.get('new_password')
        try:
            try:
                user = User.objects.get(email=email)
                reset_email = email
            except:
                user = User.objects.get(username=email)
                reset_email = user.email
        except:
            error = '此用戶名稱/電郵地址沒有註冊過。'
            return render(request, 'password-forgot.html', {'error': error})
        user.set_password(new_password)
        user.save()
        send_notification_email(reset_email,

        f'''
你好，

你的密碼已被更改。新密碼是：{new_password}。請使用新密碼登入。

謝謝你使用AnalyticX。

此致，
AnalyticX團隊                            
    ''',

        'AnalyticX 通知 - 密碼已更改',

        'Changed PW')
        return render(request, 'login.html')

    return render(request, 'password-forgot.html')

def user_logout(request):
    logout(request)
    return redirect('index')


def make_order(request):
    if request.method == 'POST':
        current_user = request.user.username
        user_email_lists = list(User_Email.objects.values_list('user_email', flat=True))
        user_email_user_list = list(User_Email.objects.values_list('user_name', flat=True))
        try:
            user_email_object = User_Email.objects.get(user_name=current_user)
            user_email_assigned = user_email_object.user_email.replace('@sales-analyticx-partner.com', "", 1)
            user_email_object_end_date = user_email_object.email_end_date
            now_date = datetime.datetime.now().date()
            if user_email_object_end_date == None or user_email_object_end_date < now_date:
                user_email_action = 'change_email'
                request.session['user_email_action'] = user_email_action

            elif user_email_object_end_date > now_date:
                date_difference = user_email_object_end_date - now_date
                if date_difference >= timedelta(days=14):
                    user_email_action = 'nothing'
                    request.session['user_email_action'] = user_email_action
                elif date_difference < timedelta(days=14) and date_difference > timedelta(days=0):
                    user_email_action = 'update'
                    request.session['user_email_action'] = user_email_action

        except User_Email.DoesNotExist:
            user_email_assigned = None
            user_email_action = "create"
            request.session['user_email_action'] = user_email_action
        print(request.POST)
        print(request.FILES)
        if 'order_nature' in request.POST:
            if request.POST.get('order_nature') == 'main':
                nature = 'main'
                following = 0
                request.session['nature'] = nature
                request.session['following'] = following
                return render(request, 'order.html',
                              {'nature': nature, 'following': following, 'user_email_assigned': user_email_assigned
                               })

            else:
                nature = request.POST.get('order_nature')
                following = request.POST.get('following')
                target_order = Order.objects.filter(order_id=following)
                chin_pitch = target_order[0].chin_pitch
                eng_pitch = target_order[0].eng_pitch
                chin_title = target_order[0].chin_title
                eng_title = target_order[0].eng_title
                website = target_order[0].website
                function = target_order[0].order_function
                pitch_img = target_order[0].pitch_img
                request.session['nature'] = nature
                request.session['following'] = following
                return render(request, 'order.html',
                              {'nature': nature, 'following': following,'user_email_assigned': user_email_assigned,
                               'user_email_action': user_email_action, 'chin_pitch': chin_pitch, 'eng_pitch': eng_pitch,
                               'chin_title': chin_title, 'eng_title': eng_title, 'website': website, 'function': function, 'pitch_img': pitch_img})



        else:
            nature = request.session.get('nature', 'False')
            following = request.session.get('following', 'False')
            form = OrderForm(request.POST, request.FILES)
            keyword = request.POST.get('keyword')
            chin_client_describe = request.POST.get('chin_client_describe')
            eng_client_describe = request.POST.get('eng_client_describe')
            chin_pitch = request.POST.get('chin_pitch')
            eng_pitch = request.POST.get('eng_pitch')
            website = request.POST.get('website')
            chin_title = request.POST.get('chin_email_title')
            eng_title = request.POST.get('eng_email_title')
            function = request.POST.get('function')
            eng_comp_name = request.POST.get('eng_comp_name')
            chin_comp_name = request.POST.get('chin_comp_name')

            chin_pitch = chin_pitch + '\n' + '\n' + chin_comp_name
            eng_pitch = eng_pitch + '\n' + '\n' + eng_comp_name

            user_email_proposed_email = str(request.POST.get('email_assigned')) + '@sales-analyticx-partner.com'
            if user_email_proposed_email in user_email_lists:
                index = user_email_lists.index(user_email_proposed_email)
                if user_email_user_list[index] != current_user:
                    error_message = "這個email 已被人使用，請再選擇一個Email。"
                    return render(request, 'order.html', {'user_email_assigned': user_email_assigned,
                                                          'error_message': error_message, 'form': form, 'nature': nature,
                                                          'following': following})

            if user_email_proposed_email == '@sales-analyticx-partner.com':
                error_message = "請輸入一個Email。"
                return render(request, 'order.html', {'user_email_assigned': user_email_assigned,
                                                      'error_message': error_message, 'form': form, 'nature': nature,
                                                      'following': following})


            if form.is_valid():
                order = form.save(commit=False)
                order.order_user = request.user
                order.order_username = request.user.username
                email = request.user.email
                user = str(order.order_user)
                order.order_amount = None
                order.order_status = 'Waiting scrapping'
                order.keyword = keyword
                order.chin_client_describe = chin_client_describe
                order.eng_client_describe = eng_client_describe
                order.chin_pitch = chin_pitch
                order.eng_pitch = eng_pitch
                order.website = website
                order.chin_title = chin_title
                order.eng_title = eng_title
                order.order_nature = nature
                order.order_following = following
                uploaded_file = request.FILES.get('pitch_img')
                max_order_id = Order.objects.all().aggregate(Max('order_id'))['order_id__max']
                if max_order_id == None:
                    max_order_id = 0
                else:
                    max_order_id = max_order_id
                if uploaded_file:
                    fs = FileSystemStorage('static/pitch_img')
                    _, file_extension = os.path.splitext(uploaded_file.name)
                    specific_file_name = f'pitch_img_{max_order_id+1}{file_extension}'
                    filename = fs.save(specific_file_name, uploaded_file)
                    uploaded_file_url = fs.url(filename)
                    order.pitch_img = uploaded_file_url
                    order.pitch_img_name = specific_file_name
                if function == 'prospect':
                    order.order_function = 'Prospecting'
                elif function == 'prospect_and_email':
                    order.order_function = 'Cold email'
                order.save()
                if request.session['user_email_action'] == 'create' and user_email_proposed_email != 'Default@sales-analyticx-partner.com':
                    user_email_object = User_Email(user_name = current_user, user_email= user_email_proposed_email,
                                                   user_email_password = None, action_for_email = 'create',
                                                   email_start_date = None, email_end_date = None, login_email_password = None)
                    user_email_object.save()

                elif request.session['user_email_action'] == 'change_email' and user_email_proposed_email != 'Default@sales-analyticx-partner.com':
                    user_email_object = User_Email.objects.get(user_name=current_user)
                    user_email_object.user_email = user_email_proposed_email
                    user_email_object.action_for_email = 'create'
                    user_email_object.user_email_password = None
                    user_email_object.email_start_date = None
                    user_email_object.email_end_date = None
                    user_email_object.login_email_password = None
                    user_email_object.save()

                elif request.session['user_email_action'] == 'update' and user_email_proposed_email != 'Default@sales-analyticx-partner.com':
                    user_email_object = User_Email.objects.get(user_name=current_user)
                    if user_email_object.user_email == user_email_proposed_email:
                        user_email_object.action_for_email = 'update'
                    elif user_email_object.user_email != user_email_proposed_email:
                        target_user_email_object = User_Email.objects.get(user_email=user_email_object.user_email)
                        target_user_email_object.delete()
                        user_email_object = User_Email(user_name=current_user, user_email=user_email_proposed_email,
                                                       user_email_password=None, action_for_email='create',
                                                       email_start_date=None, email_end_date=None, login_email_password=None)
                    user_email_object.save()

                elif request.session['user_email_action'] == 'nothing' and user_email_proposed_email != 'Default@sales-analyticx-partner.com':
                    user_email_object = User_Email.objects.get(user_name=current_user)
                    if user_email_object.user_email == user_email_proposed_email:
                        pass
                    elif user_email_object.user_email != user_email_proposed_email:
                        target_user_email_object = User_Email.objects.get(user_email=user_email_object.user_email)
                        target_user_email_object.delete()
                        user_email_object = User_Email(user_name=current_user, user_email=user_email_proposed_email,
                                                       user_email_password=None, action_for_email='create',
                                                       email_start_date=None, email_end_date=None, login_email_password=None)
                    user_email_object.save()

                this_order = int(order.order_id)

                ip_lst = ip_address.objects.all()

                ip_avg_diff = {}

                ip_len = {}

                for index, ip in enumerate(ip_lst):
                    if len(ip.start_schedule) == 0:
                        target_ip = ip.ip_name
                        target_ip_object = ip
                        ip_avg_diff = {}
                        ip_len = {}

                    else:
                        ip_len[ip.ip_name] = len(ip.start_schedule)
                        if len(ip.start_schedule) >= 4:
                            avg_diff = ((ip.start_schedule[-1]-ip.start_schedule[-2]) + (ip.start_schedule[-2]-ip.start_schedule[-3]))/2
                            ip_avg_diff[ip.ip_name] = avg_diff



                if ip_len and all(value == list(ip_len.values())[0] for value in ip_len.values()) == False:
                    min_value = min(ip_len.values())
                    print('a')
                    target_ip = [key for key, value in ip_len.items() if value == min_value][0]
                    target_ip_object = ip_address.objects.get(ip_name=target_ip)
                    print(target_ip)
                    if ip_address.objects.get(ip_name='main').sleeping == False:
                        target_ip = 'main'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                    elif ip_address.objects.get(ip_name='worker1').sleeping == False:
                        target_ip = 'worker1'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                    elif ip_address.objects.get(ip_name='main').sleeping == True and ip_address.objects.get(ip_name='worker1').sleeping == True:
                        target_ip = 'main'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)


                elif ip_len and all(value == list(ip_len.values())[0] for value in ip_len.values()) == True and all(value < 4 for value in ip_len.values()) == True:
                    if ip_address.objects.get(ip_name='main').sleeping == False:
                        target_ip = 'main'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        print('b')
                        print(target_ip)
                    elif ip_address.objects.get(ip_name='worker1').sleeping == False:
                        target_ip = 'worker1'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        print('b')
                        print(target_ip)

                    elif ip_address.objects.get(ip_name='main').sleeping == True and ip_address.objects.get(ip_name='worker1').sleeping == True:
                        target_ip = 'main'
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        print('b')
                        print(target_ip)

                elif ip_avg_diff:
                    print('c')
                    main = ip_address.objects.get(ip_name='main').sleeping
                    worker1 = ip_address.objects.get(ip_name='worker1').sleeping
                    def get_key(ip_avg_diff):
                        max_value = max(ip_avg_diff.values())
                        target_ip = [key for key, value in ip_avg_diff.items() if value == max_value][0]
                        target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        return target_ip, target_ip_object
                    target_ip, target_ip_object = get_key(ip_avg_diff)
                    if target_ip_object.sleeping == True:
                        if target_ip == 'main' and worker1 == False:
                            target_ip = 'worker1'
                            target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        elif target_ip == 'worker1' and main == False:
                            target_ip = 'main'
                            target_ip_object = ip_address.objects.get(ip_name=target_ip)
                        else:
                            target_ip = 'main'
                            target_ip_object = ip_address.objects.get(ip_name=target_ip)


                    print(target_ip)

                if target_ip == 'main':
                    ip_object = ip_address.objects.get(ip_name=target_ip)
                    if len(ip_object.start_schedule) == 4:
                        current_order_time = timezone.now()
                        print(current_order_time)
                        print(ip_object.start_schedule[0])
                        print(abs(current_order_time - ip_object.start_schedule[0]))
                        if abs(current_order_time - ip_object.start_schedule[0]) <= timedelta(minutes=60):
                            print('main order intensity too high, sleep for 30 min')
                            point_five_hr_sleep.delay()
                            count_down = main_count_down()
                            count_down.main_end_date_time = timezone.now() + timedelta(seconds=1800)
                            count_down.save()
                            target_ip_object.start_schedule.append(timezone.now()+ timedelta(minutes=30))
                            target_ip_object.start_occupied = True
                            target_ip_object.start_schedule.pop(0)
                            target_ip_object.save()
                            start.delay(user, keyword, email, this_order, about_us, contact_us, function,
                                        nature, following, target_ip)
                            return render(request, 'scrapping.html')

                        else:
                            print('main order intensity normal')
                            target_ip_object.start_schedule.append(timezone.now())
                            target_ip_object.start_occupied = True
                            target_ip_object.start_schedule.pop(0)
                            target_ip_object.save()
                            start.delay(user, keyword, email, this_order, about_us, contact_us, function,
                                        nature, following, target_ip)
                            return render(request, 'scrapping.html')

                    else:
                        print('main order start')
                        target_ip_object.start_schedule.append(timezone.now())
                        target_ip_object.start_occupied = True
                        target_ip_object.save()
                        start.delay(user, keyword, email, this_order, about_us, contact_us, function, nature,
                                    following, target_ip)
                        return render(request, 'scrapping.html')



                elif target_ip == 'worker1':
                    data = {
                        'user': user,
                        'keyword': keyword,
                        'email': email,
                        'this_order': this_order,
                        'about_us': about_us,
                        'contact_us': contact_us,
                        'function': function,
                        'nature': nature,
                        'following': following,
                        'target_ip': target_ip
                    }

                    ssh = paramiko.SSHClient()
                    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    ssh.connect('128.199.161.124', username='root', password='mAtt20040807@a')
                    sftp = ssh.open_sftp()
                    sftp.put(f'/var/www/analyticx_no_spacy/static/pitch_img/{specific_file_name}',
                             f'/var/www/worker/worker/static/pitch_img/{specific_file_name}')
                    sftp.close()
                    ssh.close()

                    ip_object = ip_address.objects.get(ip_name=target_ip)
                    if len(ip_object.start_schedule) == 4:
                        current_order_time = timezone.now()
                        print(current_order_time)
                        if abs(current_order_time - ip_object.start_schedule[0]) <= timedelta(minutes=60):
                            print('worker1 order intensity too high, sleep for 30 min')
                            requests.post('http://128.199.161.124:8000/point_five_hr_sleep')
                            count_down = worker1_count_down()
                            count_down.worker1_end_date_time = timezone.now() + timedelta(seconds=1800)
                            count_down.save()
                            target_ip_object.start_schedule.append(timezone.now() + timedelta(minutes=30))
                            target_ip_object.start_occupied = True
                            target_ip_object.start_schedule.pop(0)
                            target_ip_object.save()
                            requests.post('http://128.199.161.124:8000/start', data=data)
                            return render(request, 'scrapping.html')
                        else:
                            print('worker1 order intensity normal, start scrapping')
                            target_ip_object.start_schedule.append(timezone.now())
                            target_ip_object.start_occupied = True
                            target_ip_object.start_schedule.pop(0)
                            target_ip_object.save()
                            requests.post('http://128.199.161.124:8000/start', data=data)
                            return render(request, 'scrapping.html')

                    else:
                        print('worker1 order start')
                        target_ip_object.start_schedule.append(timezone.now())
                        target_ip_object.start_occupied = True
                        target_ip_object.save()
                        requests.post('http://128.199.161.124:8000/start', data=data)
                        return render(request, 'scrapping.html')

            elif keyword == "" or chin_client_describe == "" or eng_client_describe == '' or chin_pitch == '' or eng_pitch == '' :
                error_message = "請填寫所有資料。"
                return render(request, 'order.html', {'user_email_assigned': user_email_assigned,
                                                      'error_message': error_message, 'form': form, 'nature': nature, 'following': following})

            elif len(chin_pitch) > 200 or len(eng_pitch) > 200:
                error_message = "電郵範本字數限制為200字。"
                return render(request, 'order.html', {'user_email_assigned': user_email_assigned,
                                                      'error_message': error_message, 'form': form, 'nature': nature, 'following': following})







def download_report(request):
    if request.method == 'POST':
        order_id = request.POST.get('order_id')
        order = Order.objects.get(order_id=order_id)
        json_data = json.loads(order.json_file.read())
        keyword = order.keyword
        username = order.order_user
        status = order.order_status
        buffer = create_pdf(json_data, order_id, keyword, username)
        if status == 'Waiting payments':
            new_buffer = create_blur_pdf(buffer)
            response = FileResponse(new_buffer, content_type='application/pdf')
            response['Content-Disposition'] = f'attachment; filename="report_{order_id}.pdf"'
            return response
        response = FileResponse(buffer, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename="report_{order_id}.pdf"'
        return response
    else:
        # Handle GET request or return an error message
        pass

def download_json(request):
    if request.method == 'POST':
        order_id = request.POST.get('order_id')
        order = Order.objects.get(order_id=order_id)
        json_data = json.loads(order.json_file.read())
        for y, x in enumerate(json_data):
            if x['similarity_score'] < 0.5:
                json_data.pop(y)
                json_data.insert(0, x)
            if x['email'] == [] and x['similarity_score'] < 0.5:
                json_data.pop(y)
                json_data.insert(0, x)
        json_str = json.dumps(json_data, indent=2)
        response = HttpResponse(json_str, content_type='application/json')
        response['Content-Disposition'] = f'attachment; filename="json_data_{order_id}.json"'

        return response
    else:
        # Handle GET request or return an error message
        pass


def download_invoice(request):
    if request.method == 'POST':
        order_id = request.POST.get('order_id')
        order = Order.objects.get(order_id=order_id)
        amount = order.order_amount
        keyword = order.keyword
        username = order.order_user
        url_len = order.url_list_len
        buffer = create_invoice(order_id, amount, keyword, username, url_len)
        # Return PDF as a response
        response = FileResponse(buffer, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename="Receipt_{order_id}.pdf"'

        return response
    else:
        # Handle GET request or return an error message
        pass


def payment(request):
    order_id = request.POST.get('order_id')
    if request.method == 'POST':
        if 'payment_button' in request.POST:
            order = Order.objects.get(order_id=order_id)
            payment_proof_code = generate_verification_code()
            order.payment_proof_code = payment_proof_code
            amount = order.order_amount
            order.order_status = 'Payments confirm'
            order.save()
            return render(request, 'payment.html', {'order_id': order_id, 'payment_proof_code': payment_proof_code, 'amount': amount})
        else:
            send_notification_email( 'analyticx01@gmail.com',
                                    f'{order_id} payment has been made, please check payme/alipay',
                                    f'{order_id} payment has been made, please check payme/alipay',
                                    f'notfied admin {order_id} has paid')
            return render(request, 'generating_report.html')


def start_email(request):
    if request.method == 'POST':
        print(request.POST)
        if 'start_order' in request.POST:
            order_id = request.POST.get('order_id')
            target_order = Order.objects.get(order_id=order_id)
            target_user = target_order.order_user
            function = target_order.order_function
            target_order.order_status = 'Waiting production'
            target_order.save()
            user = str(target_user)
            target_email = target_user.email
            print(target_email)
            scrape.delay(order_id, about_us, contact_us, function)

            return render(request, 'generating_report.html')


        elif 'scraping_email_assigned' in request.POST:
            order_id = request.POST.get('order_id')
            scrapping_email = request.POST.get('scraping_email_assigned')
            scrapping_email_password = request.POST.get('scraping_email_password')
            login_email_password = request.POST.get('login_email_password')
            order = Order.objects.get(order_id=order_id)
            order.scraping_email_assigned = scrapping_email
            order.scrapping_email_password = scrapping_email_password
            order.login_email_password = login_email_password
            order.scraping_email_assigned_date = datetime.datetime.now().strftime("%Y-%m-%d")
            order.order_status = 'Waiting emails'
            all_ip = ip_address.objects.all()
            keep_record = {}
            for ip in all_ip:
                if ip.email_occupied == False:
                    order.assigned_ip_object = ip.ip_name
                    break
                keep_record[ip.ip_name] = sum(ip.email_schedule)
            if order.assigned_ip_object == None:
                min_key = min(keep_record, key=keep_record.get)
                order.assigned_ip_object = min_key
            order.save()

            total_url_len = order.url_list_len
            target_ip = ip_address.objects.get(ip_name=order.assigned_ip_object)
            if total_url_len <= 10:
                total_days_required = 1
            elif (total_url_len - 10) <= 20:
                total_days_required = 2
            elif (total_url_len - 10 - 20) <= 40:
                total_days_required = 3
            elif (total_url_len - 10 - 20 - 40) <= 80:
                total_days_required = 4
            elif (total_url_len - 10 - 20 - 40 - 80) <= 0:
                total_days_required = 4 + (total_url_len - 10 - 20 - 40 - 80) % 80
            target_ip.email_schedule.append(total_days_required)
            target_ip.email_occupied = True
            target_ip.save()


            target_user_email_object = User_Email.objects.get(user_email=order.scraping_email_assigned)
            target_user_email_object.action_for_email = 'nothing'
            target_user_email_object.email_start_date = datetime.datetime.now()
            target_user_email_object.email_end_date = datetime.datetime.now() + datetime.timedelta(days=30)
            target_user_email_object.user_email_password = order.scrapping_email_password
            target_user_email_object.login_email_password = order.login_email_password
            target_user_email_object.save()


            target_user = order.order_user
            target_email = target_user.email
            url_len = order.url_list_len
            send_notification_email(target_email,
                                    f'''
                        {target_user}你好，

                        目標行業的分析報告已完成。總共獲取了{url_len}條信息。你可以前往我們網址中'我的訂單'預覽該行業的公司。

                        analyticx.online

                        你可以在報告中找到公司名稱，電郵，電話，網址與相似性4項結果。我們會以每天10-100個的速度Cold Email這批公司。你可以使用我們提供的電郵查看這些公司的回復。

                        謝謝你使用AnalyticX。

                        此致，
                        AnalyticX團隊                            
                            ''',
                                'AnalyticX 通知 - 報告已完成',
                                'Step 2 email is sent')
            if request.user.is_authenticated:
                username = request.user.username
                return render(request, "index-8.html", {'username': username})

        elif 'download_report' in request.POST:
            order_id = request.POST.get('order_id')
            order = Order.objects.get(order_id=order_id)
            final_report = order.json_file
            response = HttpResponse(final_report)
            response['Content-Disposition'] = f'attachment; filename="json_data_{order_id}.json"'
            return response
from django.http import JsonResponse

@csrf_exempt
def recieve_url_list(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        url_list = data['url_list']
        order_id = data['order_id']
        json_list = data['json_list']
        order = Order.objects.get(order_id=order_id)
        filename = f'json_data_{order_id}.json'
        file_path = os.path.join(settings.STATICFILES_DIRS[0], 'json_files', filename)

        url_filename = f'url_list_{order_id}.json'
        url_file_path = os.path.join(settings.STATICFILES_DIRS[0], 'url_list', url_filename)

        with open(file_path, 'w') as f:
            f.write(json.dumps(json_list))
            order.json_file.name = os.path.join('static', 'json_files', filename)

        with open(url_file_path, 'w') as f:
            f.write(json.dumps(url_list))
            order.url_list.name = os.path.join('static', 'url_list', url_filename)
        order.order_status = 'Waiting payments'
        order.save()
        return JsonResponse({'status': 'ok'})










