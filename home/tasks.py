import spacy
from qexal.celery import app
from selenium import webdriver
from items.models import Order, User_Email, ip_address
from django.contrib.auth.models import User
import json
from api.scrape import api_scrape
from api.send_notification_email import send_notification_email, send_client_email
from api.spin_email import spin_chin, spin_eng
from api.turn_page import turn_google_page
from selenium.webdriver.chrome.options import Options
import random
from datetime import datetime, timedelta
import os
from django.conf import settings
import time
from api.new_scrapper.new_scrapper.spider import MySpider
from selenium.common.exceptions import WebDriverException
import requests

chin_email_title = []
eng_email_title = []

@app.task(queue = 'start')
def point_five_hr_sleep():
    main_ip = ip_address.objects.get(ip_name='main')
    main_ip.sleeping = True
    main_ip.save()
    time.sleep(10)
    main_ip.sleeping = False
    main_ip.save()
    print('1.5 hour sleep ended')

@app.task(queue = 'start')
def emergent_sleep():
    main_ip = ip_address.objects.get(ip_name='main')
    main_ip.sleeping = True
    main_ip.save()
    time.sleep(10)
    main_ip.sleeping = False
    main_ip.save()
    print('emergent sleep ended')



@app.task(queue='start')
def start(user, keyword, recipient, input_id, about_us, contact_us, function, nature, following, target_ip):
    eng_nlp = spacy.load('en_core_web_sm')
    chin_nlp = spacy.load('zh_core_web_sm')
    options = Options()
    options.binary_location = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
    #options.add_argument('--remote-debugging-port=9222')
    #options.add_argument('--no-sandbox')
    #options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-crash-reporter')
    #options.add_argument('--user-data-dir=/home/root/chrome_data')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument('--disable-software-rasterizer')
    driver = webdriver.Chrome('/Users/wailokkwok/Downloads/chromedriver_mac64/chromedriver', options=options)
    driver.set_page_load_timeout(30)
    print('started')
    print('entered')
    keyword_list = [f"intitle:'{str(keyword)}' + intitle:'關於我們'", f"intitle:'{str(keyword)}'+ intitle:'聯絡我們'",f"intitle:'{str(keyword)}'+ intitle:'聯繫我們'"]
    result = []
    pattern = r"^(https?://[^/]+)"
    count = 0
    for x in keyword_list:
        turn_google_page(x, pattern, driver, count, result)

    print('finished step1')

    if nature != 'main':
        order = Order.objects.get(order_id=following)
        target_url_lst = order.url_list
        with target_url_lst.open('r') as f:
             folowing_url_lst = f.read()
        for url in result:
            if url in folowing_url_lst:
                result.remove(url)

    order = Order.objects.get(order_id=input_id)
    target_ip = ip_address.objects.get(ip_name=target_ip)
    if len(result) == 0:
        print('step 1 scrapping no result')
        send_notification_email('analyticx01@gmail.com', f'order {order} reached limit, hit stop button in admin page. The keyword is {order.keyword}', f'order{order} ran into limit', 'notified admin to handle limit reaching')
        send_notification_email(recipient,
                                f'''
            {user}你好，

            不好意思，我們未能根據你的目標行業{order.keyword}找到資料。請你重新以另一個字眼下單 e.g. 食材供應 --> 食品供應商，marketing --> 營銷。
            
            如果你嘗試多次後也未能下單，請與客服聯絡，我們會查看問題所再並協助你重新下單。

            謝謝你使用AnalyticX。

            此致，
            AnalyticX團隊                            
                                    ''',

                                'AnalyticX 通知 - 我們成功收集資料',
                                'Step 1 email is sent')
        driver.quit()
        target_ip.start_occupied = False
        target_ip.save()
        return None
    else:
        order.order_status = 'Waiting payments'
        if function == "prospect":
            order.order_amount = len(result)*2
        elif function == "prospect_and_email":
            order.order_amount = len(result)*3
        order.scraping_email_assigned = ''
        client_chin = order.chin_client_describe
        client_eng = order.eng_client_describe
        try:
            init_report = api_scrape(MySpider,driver, about_us, contact_us, 15, result, client_chin, client_eng, chin_nlp, eng_nlp)
        except WebDriverException:
            send_notification_email('analyticx01@gmail.com',
                                    f'order {order} encountered web driver exception. This is the first time',
                                    f'order{order} web driver exception',
                                    'notified admin to handle web driver exception')
            try:
                init_report = api_scrape(MySpider, driver, about_us, contact_us, 15, result, client_chin, client_eng,
                                         chin_nlp, eng_nlp)
            except WebDriverException:
                send_notification_email('analyticx01@gmail.com',
                                        f'order {order} encountered web driver exception. This is the second time',
                                        f'order{order} web driver exception',
                                        'notified admin to handle web driver exception')
        filtered_init_report = []
        for data in init_report:
            if data['公司email'] != []:
                filtered_init_report.append(data)
                print(data['目標相似評分'])
        sorted_list = sorted(filtered_init_report, key=lambda x: x['目標相似評分'], reverse=True)[0:6]
        dummy = [{'公司名稱':'@@@@@', '公司email':'@@@@@@@@@@@@@@@@@@@@@@@@@@@', '公司電話':'@@@@@@@@@@@@@@@@@', '目標相似評分':'@@@@','網址語言':'@'},
                 {'公司名稱':'@@@@@@@@@', '公司email':'', '公司電話':'@@@@@@@@@@@@@@@@@', '目標相似評分':'@@@@', '網址語言':'@'},
                 {'公司名稱':'@', '公司email':'@@@@@@@@@@@@@@@@@@@@@@@@@@@', '公司電話':'', '目標相似評分':'@@@@','網址語言':'@'},
                 {'公司名稱':'@@@@@@@@@@@@', '公司email':'', '公司電話':'', '目標相似評分':'@@@@', '網址語言':'@'},
                 {'公司名稱':'@@@@@', '公司email':'@@@@@@@@@@@@@@@@@@@@@@@@@@@@', '公司電話':'@@@@@@@@@', '目標相似評分':'@@@@','網址語言':'@'}]
        for i in range(len(result)-5):
            sorted_list.append(random.choice(dummy))

        filename = f'json_data_{input_id}.json'
        file_path = os.path.join(settings.STATICFILES_DIRS[0], 'json_files', filename)

        with open(file_path, 'w') as f:
            f.write(json.dumps(sorted_list))
            order.json_file.name = os.path.join('static', 'json_files', filename)

        url_filename = f'url_list_{input_id}.json'
        url_file_path = os.path.join(settings.STATICFILES_DIRS[0], 'url_list', url_filename)
        with open(url_file_path, 'w') as f:
            f.write(json.dumps(result))
            order.url_list.name = os.path.join('static', 'url_list', url_filename)
            print(order.url_list.name)
            order.order_status = 'Waiting payments'
            order.save()


        send_notification_email( recipient,
                            f'''
    {user}你好，
    
    我們已成功幫你分析了目標行業的詳情。總共獲取了{len(result)}條信息。你可以前往我們網址中'我的訂單'預覽該行業的公司。
    
    analyticx.online
    
    在預覽了該行業的公司後，你可以選擇是否要繼續下一步。如果你想繼續，你可以按下付款鍵，我們會在約24-48小時後提供完整行業報告，並向他們開始發送Cold Email。
    
    謝謝你使用AnalyticX。
    
    此致，
    AnalyticX團隊                            
                            ''',

                                'AnalyticX 通知 - 我們成功收集資料',
                                'Step 1 email is sent')
        print('order_updated')
        target_ip.start_occupied = False
        target_ip.save()
        driver.quit()





@app.task(queue='scrape')
def scrape(order_id, about_us, contact_us, function):
    eng_nlp = spacy.load('en_core_web_sm')
    chin_nlp = spacy.load('zh_core_web_sm')
    options = Options()
    # options.add_argument('--remote-debugging-port=9222')
    # options.add_argument('--no-sandbox')
    options.binary_location = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-crash-reporter')
    # options.add_argument('--user-data-dir=/home/root/chrome_data')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument('--disable-software-rasterizer')
    driver = webdriver.Chrome('/Users/wailokkwok/Downloads/chromedriver_mac64/chromedriver', options=options)
    driver.set_page_load_timeout(30)
    print('started2')
    order = Order.objects.get(order_id=order_id)
    client_chin = order.chin_client_describe
    client_eng = order.eng_client_describe
    with open(order.url_list.path, 'r') as file:
        url_list = json.load(file)
    order.order_status = 'Waiting production'
    order.save()
    print('Waiting production')
    try:
        report = api_scrape(MySpider, driver, about_us, contact_us, len(url_list), url_list, client_chin, client_eng, chin_nlp,
                                 eng_nlp)
    except WebDriverException:
        send_notification_email('analyticx01@gmail.com',
                                f'order {order} encountered web driver exception. This is the first time',
                                f'order{order} web driver exception',
                                'notified admin to handle web driver exception')
        try:
            report = api_scrape(MySpider, driver, about_us, contact_us, len(url_list), url_list, client_chin, client_eng,
                                     chin_nlp, eng_nlp)
        except WebDriverException:
            send_notification_email('analyticx01@gmail.com',
                                    f'order {order} encountered web driver exception. This is the second time',
                                    f'order{order} web driver exception',
                                    'notified admin to handle web driver exception')

    print('finished step2')

    target_user = order.order_user
    target_user_email_object = User_Email.objects.get(user_name=target_user)
    user_email_object_action = target_user_email_object.action_for_email

    if user_email_object_action == 'create':
        send_notification_email('analyticx01@gmail.com', f'{order_id} need to create a zoho acc',
                            f'Order_{order_id} has finished scrapping, create zoho email acc', f'Notified admin {order_id} scrapping has complete')
    elif user_email_object_action == 'nothing':
        send_notification_email('analyticx01@gmail.com', f'{order_id} just need to press start in all orders',
                                f'Order_{order_id} has finished scrapping, press start in all orders',
                                f'Notified admin {order_id} scrapping has complete')
    elif user_email_object_action == 'update':
        send_notification_email('analyticx01@gmail.com', f'{order_id} need to extend a month in zoho',
                                f'Order_{order_id} has finished scrapping, extend one month in zoho',
                                f'Notified admin {order_id} scrapping has complete')


    print('running report')
    filename = f'json_data_{order_id}.json'
    file_path = os.path.join(settings.STATICFILES_DIRS[0], 'json_files', filename)

    with open(file_path, 'w') as f:
        f.write(json.dumps(report))
        order.json_file.name = os.path.join('static', 'json_files', filename)
        order.save()

    if function == 'Prospecting':
        order.order_status = 'Finished'
        order.save()
        target_user = order.order_user
        email = target_user.email
        send_notification_email( email,
                            f'''
    {target_user}你好，      
    
    我們已成功幫你分析了目標行業的詳情。總共獲取了{len(report)}條信息。你可以前往我們網址中'我的訂單'預覽該行業的公司。
    
    analyticx.online
    
    你可以在報告中找到公司名稱，電郵，電話，網址與相似性4項結果。
    
    謝謝你使用AnalyticX。
    
    此致，
    AnalyticX團隊                            
                            ''',

                                'AnalyticX 通知 - 我們成功收集資料',
                                'Step 1 email is sent')
    else:
        data = {'report': report, 'order_id': order_id}
        requests.post('http://128.199.161.124:8000/recieve_final_report', json = data)
        order.order_status = 'Waiting email assignment'
        order.url_list_len = len(url_list)
        order.save()
        print('finish_order')



@app.task(queue='start')
def send_mass_email(source_email, source_password, target_emails,email_title, email_content, order_id):
    try:
        send_client_email(source_email, source_password,['wlkwokab@connect.ust.hk', 'matthewkwok181200@gmail.com'], email_content,email_title,order_id, f'{order_id} is sent')

    except:
        time.sleep(5)
        send_client_email(source_email, source_password, ['wlkwokab@connect.ust.hk', 'matthewkwok181200@gmail.com'], email_content,email_title,order_id, f'{order_id} is sent')

    order = Order.objects.get(order_id=order_id)
    order.order_status = 'Finished'
    order.save()

@app.task(queue='start')
def del_order():
    all_orders = Order.objects.all().order_by('order_id').filter(order_status='Finished')
    if len(all_orders) != 0:
        target_order = all_orders[0]
        today = datetime.today().date()
        target_order_id = target_order.order_id
        if target_order.order_date - today == 60:
            target_order.delete()
            json_data_path = f'static/json_files/json_data_{target_order_id}.json'
            os.remove(json_data_path)
            url_list_path = f'static/url_list/url_list_{target_order_id}.json'
            os.remove(url_list_path)
    elif len(all_orders) == 0:
        print('no order to delete')





@app.task(queue='start')
def daily_send_emails():
    def email_reptition(run_per_day, day, range1, range2,source_email, source_password, target_list, starting_point, target_order_id,
                        chin_email_title, chin_email_content, eng_email_title, eng_email_content, target_order, target_ip):
        if (len(target_list) - starting_point) > run_per_day:
            print('run 1')
            for i in range(run_per_day+1):
                random_number = random.uniform(range1, range2)
                scheduled_time = datetime.utcnow() + timedelta(minutes=5) + timedelta(minutes=random_number)
                task_name = f'send_email_day{day}_{i}'
                if target_list[starting_point + i]['網址語言'] == 'chin' or target_list[starting_point + i][
                    '網址語言'] == None:
                    if target_list[starting_point + i]['公司email'] != []:
                        print(target_list[starting_point + i])
                        print(target_list[starting_point + i]['公司名稱'])
                        print(target_list[starting_point + i]['公司email'][0])
                        c_email_content = target_list[starting_point + i]['公司名稱'] +  '\n' + '\n' + chin_email_content
                        send_email.apply_async(
                            kwargs={'source_email':source_email, 'source_password':source_password,
                                    'target_email': target_list[starting_point + i]['公司email'][0],
                                    'email_title': chin_email_title, 'email_content': c_email_content, 'lang': 'chin', 'email_num':f'day{day}_{i}', 'order_id':target_order_id,
                                    'company_name':target_list[starting_point + i]['公司名稱']}
                            , eta=scheduled_time, task_id=task_name)
                        target_order.email_progress += 1
                    else:
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has no email')

                elif target_list[starting_point + i]['網址語言'] == 'eng':
                    if target_list[starting_point + i]['公司email'] != []:
                        print(target_list[starting_point + i])
                        print(target_list[starting_point + i]['公司名稱'])
                        print(target_list[starting_point + i]['公司email'][0])
                        e_email_content = target_list[starting_point + i]['公司名稱'] + '\n' + '\n' + eng_email_content
                        send_email.apply_async(
                            kwargs={'source_email':source_email, 'source_password':source_password,
                                    'target_email': target_list[starting_point + i]['公司email'][0],
                                    'email_title': eng_email_title, 'email_content': e_email_content,'lang': 'eng', 'email_num':f'day{day}_{i}', 'order_id':target_order_id,
                                    'company_name':target_list[starting_point + i]['公司名稱']}
                            , eta=scheduled_time, task_id=task_name)
                        target_order.email_progress += 1

                    else:
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has no email')

            target_order.sent_email_days += 1
            target_ip.email_schedule[0] -= 1
            target_order.save()
            target_ip.save()
            print("today's email scheudled")
        else:
            print('run 2')
            for i in range((len(target_list) - starting_point)):
                random_number = random.uniform(range1, range2)
                scheduled_time = datetime.utcnow() + timedelta(minutes=5) + timedelta(minutes=random_number)
                task_name = f'send_email_day{day}_{i}'
                print(target_list[int(starting_point) + i])
                if target_list[starting_point + i]['網址語言'] == 'chin':
                    if target_list[starting_point + i]['公司email'] != []:
                        print(target_list[starting_point + i])
                        print(target_list[starting_point + i]['公司名稱'])
                        print(target_list[starting_point + i]['公司email'][0])
                        c_email_content = target_list[starting_point + i][
                                                 '公司名稱'] + '\n' + '\n' + chin_email_content
                        send_email.apply_async(
                            kwargs={'source_email':source_email, 'source_password':source_password,
                                    'target_email': target_list[starting_point + i]['公司email'][0],
                                    'email_title': chin_email_title, 'email_content': c_email_content,'lang': 'chin', 'email_num':f'day{day}_{i}', 'order_id':target_order_id,
                                    'company_name':target_list[starting_point + i]['公司名稱']}
                            , eta=scheduled_time, task_id=task_name)
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has email')
                    else:
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has no email')

                elif target_list[starting_point + i]['網址語言'] == 'eng':
                    if target_list[starting_point + i]['公司email'] != []:
                        print(target_list[starting_point + i])
                        print(target_list[starting_point + i]['公司名稱'])
                        print(target_list[starting_point + i]['公司email'][0])
                        e_email_content = target_list[starting_point + i]['公司名稱'] + '\n' + '\n' + eng_email_content
                        send_email.apply_async(
                            kwargs={'source_email':source_email, 'source_password':source_password,
                                    'target_email': target_list[starting_point + i]['公司email'][0],
                                    'email_title': eng_email_title, 'email_content': e_email_content,'lang': 'eng', 'email_num':f'day{day}_{i}', 'order_id':target_order_id,
                                    'company_name':target_list[starting_point + i]['公司名稱']}
                            , eta=scheduled_time, task_id=task_name)
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has email')
                    else:
                        target_order.email_progress += 1
                        print(f'email_day{day}_{i} has no email')
            target_order.sent_email_days += 1
            target_ip.email_schedule[0] -= 1
            target_order.order_status = 'Finished'
            target_order.save()
            target_ip.email_schedule.pop(0)
            if len(target_ip.email_schedule) == 0:
                target_ip.email_occupied = False
            target_ip.save()
            target_order = Order.objects.get(order_id=target_order_id)
            target_user = User.objects.get(username=target_order.order_user)
            target_result_len = target_order.url_list_len
            email = target_user.email

            send_notification_email(email,

                                    f'''
            {target_order.order_user}你好，

            所有Cold Email已完成。我們總供Cold Email 了{target_result_len}間公司。你可以在我們提供的電郵觀測他們的回復。

            我們提供的電郵會在1個月後過期，如果你想把Email有效期延長，請與我們的客服人員聯繫。

            謝謝你使用AnalyticX。

            此致，
            AnalyticX團隊                            
                ''',

                                    'AnalyticX 通知 - Cold Email已完成',

                                    'Order Status changed to finish')
            print(f'email sending finished')

    all_orders = Order.objects.all().order_by('-order_id').filter(order_status='Waiting emails').filter(
        assigned_ip_object='main'
    )
    print(all_orders)
    print('hihihihi')
    target_order_id = all_orders[0].order_id
    print(target_order_id)
    target_order = Order.objects.get(order_id=target_order_id)
    target_ip = ip_address.objects.get(ip_name=target_order.assigned_ip_object)
    target_list = json.load(target_order.json_file)
    print(target_list)
    duplicated_target_list = target_list.copy()
    for order in duplicated_target_list:
        if order['公司email'] == None:
            duplicated_target_list.remove(order)
    print(duplicated_target_list)
    starting_point = target_order.email_progress
    print(starting_point)
    chin_email_content = target_order.chin_pitch
    eng_email_content = target_order.eng_pitch
    chin_title = target_order.chin_title
    eng_title = target_order.eng_title
    source_email = target_order.scraping_email_assigned
    source_password = target_order.scrapping_email_password
    if target_order.sent_email_days == 0:
        email_reptition(10, 0, 5, 40,source_email, source_password, duplicated_target_list, starting_point, target_order_id, chin_title, chin_email_content, eng_title, eng_email_content, target_order,target_ip)
    elif target_order.sent_email_days == 1:
        email_reptition(20,1, 5, 25,source_email, source_password,duplicated_target_list, starting_point, target_order_id, chin_title, chin_email_content, eng_title, eng_email_content, target_order,target_ip)
    elif target_order.sent_email_days == 2:
        email_reptition(40, 2, 3, 7,source_email, source_password,duplicated_target_list, starting_point, target_order_id, chin_title, chin_email_content, eng_title, eng_email_content, target_order,target_ip)
    elif target_order.sent_email_days == 3:
        email_reptition(80, 3, 2, 5,source_email, source_password,duplicated_target_list, starting_point, target_order_id, chin_title, chin_email_content, eng_title, eng_email_content, target_order,target_ip)
    else:
        email_reptition(80, 'warm_up_complete', 3, 7,source_email, source_password, duplicated_target_list, starting_point, target_order_id, chin_title,
                        chin_email_content, eng_title, eng_email_content, target_order,target_ip)


@app.task(queue='start')
def send_email(source_email, source_password, target_email,email_title, email_content, lang, email_num, order_id, company_name):

    try:
        send_client_email(source_email, source_password,'wlkwokab@connect.ust.hk', email_content,email_title,order_id, f'{email_num} is sent')
        print('target_email' + target_email)
        print('company_name' + company_name)

    except:
        time.sleep(5)
        send_client_email(source_email, source_password, 'wlkwokab@connect.ust.hk', email_content,email_title,order_id, f'{email_num} is sent')
        print('target_email' + target_email)
        print('company_name' + company_name)




@app.task(queue='start')
def say_hi():
    print('hi')





