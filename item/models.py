from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField


# Create your models here.

class main_count_down(models.Model):
    main_end_date_time = models.DateTimeField()
    main_id = models.AutoField(primary_key=True)
    def __str__(self):
        return str(self.main_id)

class worker1_count_down(models.Model):
    worker1_end_date_time = models.DateTimeField()
    worker1_id = models.AutoField(primary_key=True)
    def __str__(self):
        return str(self.worker1_id)


class ip_address(models.Model):
    ip_name = models.CharField(max_length=10, null=True, blank=True, unique=True)
    email_occupied = models.BooleanField(default=False)
    email_schedule = ArrayField(models.IntegerField(), default=list, null=True, blank=True)
    start_occupied = models.BooleanField(default=False)
    start_schedule = ArrayField(models.DateTimeField(), default=list, null=True, blank=True)
    sleeping = models.BooleanField(default=False)

    def __str__(self):
        return str(self.ip_name)




class User_Email(models.Model):
    user_email_object_id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=30, null=True, blank=True)
    user_email = models.EmailField(max_length=100, null=True, blank=True)
    user_email_password = models.TextField(max_length=30, null=True, blank=True)
    action_for_email = models.CharField(max_length=30, null=True, blank=True)
    email_start_date = models.DateField(null=True, blank=True)
    email_end_date = models.DateField(null=True, blank=True)
    login_email_password = models.TextField(max_length=30, null=True, blank=True, default=None)

    def __str__(self):
        return str(self.user_name)


class Order(models.Model):
    WAITING_SCRAPPING = 'Waiting scrapping'
    WAITING_PAYMENTS = 'Waiting payments'
    WAITING_PAYMENTS_CONFIRM = 'Waiting payments confirm'
    WAITING_PRODUCTION = 'Waiting production'
    WAITING_EMAIL_ASSIGNMENT = 'Waiting email assignment'
    WAITING_EMAIL = 'Waiting emails'
    FINISH = 'Finished'
    PROSPECTING = 'Prospecting'
    COLD_EMAIL = 'Cold email'

    FUNCTION_CHOICES = [(PROSPECTING, 'Prospecting'), (COLD_EMAIL, 'Cold email')]

    ORDER_STATUS_CHOICES = [
        (WAITING_SCRAPPING, 'Waiting scrapping'),
        (WAITING_PAYMENTS, 'Waiting payments'),
        (WAITING_PAYMENTS_CONFIRM, 'Payments confirm'),
        (WAITING_PRODUCTION, 'Waiting production'),
        (WAITING_EMAIL_ASSIGNMENT, 'Waiting email assignment'),
        (WAITING_EMAIL, 'Waiting emails'),
        (FINISH, 'Finished'),
    ]
    order_function = models.CharField(max_length=50, choices=FUNCTION_CHOICES, default=PROSPECTING, null=True, blank=True)
    order_nature = models.CharField(max_length=10, default= None, null=True, blank=True)
    order_following = models.IntegerField(default=None, null=True, blank=True)
    order_id = models.AutoField(primary_key=True)
    order_date = models.DateField(auto_now_add=True)
    order_time = models.TimeField(auto_now_add=True, null=True, blank=True)
    order_user = models.ForeignKey(User, on_delete=models.CASCADE)
    order_username = models.CharField(max_length=50, null=True, blank=True)
    order_amount = models.IntegerField(null=True, blank=True)
    order_status = models.CharField(max_length=50, choices=ORDER_STATUS_CHOICES, default=WAITING_SCRAPPING)
    json_file = models.FileField(upload_to='static/json_files/', blank=True, null=True)
    keyword = models.CharField(max_length=50)
    chin_client_describe = models.TextField(max_length=200)
    eng_client_describe = models.TextField(max_length=200)
    chin_pitch = models.TextField(max_length=2000)
    eng_pitch = models.TextField(max_length=2000)
    chin_title = models.TextField(max_length=50, blank=True, null=True)
    eng_title = models.TextField(max_length=50, blank=True, null=True)
    website = models.URLField(max_length=200, blank=True, null=True)
    pitch_img = models.FileField(upload_to='static/pitch_img/', blank=True, null=True, default = None)
    url_list = models.FileField(upload_to='static/url_list/', blank=True, null=True)
    payment_proof_code = models.TextField(max_length=20, blank=True, null=True)
    url_list_len = models.IntegerField(default=0, null=True, blank=True)
    email_progress = models.IntegerField(default=0, null=True, blank=True)
    sent_email_days = models.IntegerField(default=0, null=True, blank=True)
    scraping_email_assigned = models.TextField(max_length=50, null=True, blank=True, default=None)
    scrapping_email_password = models.TextField(max_length=50, null=True, blank=True, default=None)
    scraping_email_assigned_date = models.DateField(null=True, blank=True)
    login_email_password = models.TextField(max_length=50, null=True, blank=True, default=None)
    assigned_ip_object = models.CharField(max_length=20, null=True, blank=True, default=None)
    pitch_img_name = models.CharField(max_length=20, null=True, blank=True, default=None)
    def __str__(self):
        return str(self.order_id)


from django.db import models

# Create your models here.
