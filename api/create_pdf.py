from io import BytesIO
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
import os
from django.conf import settings
from reportlab.lib import colors
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer, Image, Flowable
from PIL import ImageFilter
from PIL import Image as PilImage
import fitz
import io

def create_pdf(data, order_id, keyword, username):
    buffer = BytesIO()

    doc = SimpleDocTemplate(buffer, pagesize=landscape(letter), rightMargin=72, leftMargin=72, topMargin=72,
                            bottomMargin=18)
    content = []

    light_blue = colors.HexColor('#ADD8E6')
    dark_blue = colors.HexColor('#4682B4')
    black = colors.black
    grey = colors.grey

    # Add company logo
    company_logo_path = os.path.join(settings.BASE_DIR, 'static', 'images', 'analyticx2.png')
    image = Image(company_logo_path, width=100, height=50)
    content.append(image)

    # Add order details
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER, fontSize=12, textColor=black))
    order_details = Paragraph(f"Order ID: {order_id} | Keyword: {keyword} | Username: {username}", styles['Center'])
    content.append(order_details)

    # Add spacer
    content.append(Spacer(1, 24))

    # Add table header
    styles = getSampleStyleSheet()
    styles['BodyText'].alignment = TA_LEFT
    styles['BodyText'].fontSize = 10
    styles['BodyText'].textColor = black
    table_data = [
        [
            Paragraph("#", styles["BodyText"]),
            Paragraph("Name", styles["BodyText"]),
            Paragraph("Email", styles["BodyText"]),
            Paragraph("Phone", styles["BodyText"]),
            Paragraph("Score", styles["BodyText"]),
        ]
    ]

    # Add table data
    row_count = 1
    for item in data:
        email_list = [email.replace(u'\xa0', ' ') for email in item["公司email"]]
        email_str = ', '.join(email_list)

        telephone_list = [str(tel) for tel in item["公司電話"]]
        telephone_str = ', '.join(telephone_list)

        table_data.append(
            [
                Paragraph(str(row_count), styles["BodyText"]),
                Paragraph(item["公司名稱"], styles["BodyText"]),
                Paragraph(email_str, styles["BodyText"]),
                Paragraph(telephone_str, styles["BodyText"]),
                Paragraph(str(item["目標相似評分"]), styles["BodyText"]),
            ]
        )
        row_count += 1

    # Create table
    table = Table(table_data, colWidths=[40, 120, 220, 170, 120])
    table.setStyle(TableStyle([
        ('BACKGROUND', (1, 0), (-1, 0), light_blue),
        ('TEXTCOLOR', (0, 0), (-1, 0), black),
        ('GRID', (0, 0), (-1, -1), 1, black),
        ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
        ('FONTSIZE', (0, 0), (-1, -1), 10),
        ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
    ]))

    content.append(table)

    doc.build(content)

    buffer.seek(0)
    return buffer




def create_blur_pdf(file, blur_radius=30):
    pdf = fitz.open(stream=file, filetype="pdf")
    page_count = len(pdf)

    page_blur_rects = [
        (0, fitz.Rect(50, 270, 750, 650)),
    ]

    for i in range(1, page_count):
        page_blur_rects.append((i, fitz.Rect(50, 0, 750, 650)))

    for page_number, blur_rect in page_blur_rects:
        page = pdf[page_number]
        zoom = 2  # Increase the zoom factor for better image quality
        matrix = fitz.Matrix(zoom, zoom)
        pixmap = page.get_pixmap(matrix=matrix, clip=blur_rect)
        region_image = PilImage.frombytes("RGB", (pixmap.width, pixmap.height), pixmap.samples, "raw", "RGB", 0, 1)
        blurred_region = region_image.filter(ImageFilter.GaussianBlur(radius=blur_radius))

        with io.BytesIO() as buffer:
            blurred_region.save(buffer, format="JPEG", optimize=True, quality=50)  # Adjust quality to control the compression level
            buffer.seek(0)

            # Overlay the blurred image on the original PDF page
            page.insert_image(blur_rect, stream=buffer, keep_proportion=True, mask=None)

    output_buffer = io.BytesIO()
    pdf.save(output_buffer, garbage=4, deflate=True)
    pdf.close()
    output_buffer.seek(0)
    return output_buffer





def create_invoice(order_id, amount, keyword, username, length_of_report):
    buffer = BytesIO()
    light_blue = colors.HexColor('#ADD8E6')
    dark_blue = colors.HexColor('#4682B4')
    black = colors.black
    grey = colors.grey

    doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=72, leftMargin=72, topMargin=72, bottomMargin=18)
    content = []

    # Company logo and information
    logo_path = os.path.join(settings.BASE_DIR, 'static', 'images', 'analyticx2.png')
    logo = Image(logo_path, width=120, height=60)
    logo.hAlign = 'LEFT'

    company_info = Paragraph(
        "AnalyticX<br/>Room 28, 9th Floor, Sing Win FTY BLDG, Sing Yip Street, Koon Tong<br/>Phone: 64963362<br/>Email: general@analyticx.online",
        getSampleStyleSheet()["Normal"],
    )

    # Invoice title, username, and order ID
    styles = getSampleStyleSheet()

    invoice_info = Paragraph(
        f'<para align="right"><font size="30"><strong>Receipt</strong></font><br/><br/><br/>Username: {username}<br/>Order ID: {order_id}<br/>Keyword: {keyword}</para>',
        styles["Normal"],
    )

    # Add a spacer between the logo and invoice_info
    spacer = Spacer(1, 24)

    # Header table with logo, spacer, company_info, and invoice_info
    header_table_data = [
        [logo, spacer, invoice_info],
        [company_info, '', '']
    ]

    content.append(Spacer(1, 24))  # Add a spacer with a height of 24

    header_table = Table(header_table_data, colWidths=[280, 20, 260])  # Adjust the colWidths accordingly
    header_table.setStyle(TableStyle([
        ('VALIGN', (0, 0), (-1, -1), 'TOP'),
    ]))

    content.append(header_table)
    content.append(Spacer(1, 24))

    # Table with order details
    data = [
        [Paragraph(f"<strong>Order ID</strong>", getSampleStyleSheet()["Normal"]),
         Paragraph(f"<strong>Report Length</strong>", getSampleStyleSheet()["Normal"]),
         Paragraph(f"<strong>Price</strong>", getSampleStyleSheet()["Normal"])],
        [order_id, length_of_report, f"${amount}"],
    ]

    table = Table(data)
    table.setStyle(TableStyle([
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('GRID', (0, 0), (-1, -1), 1, black),
        ('BACKGROUND', (0, 0), (-1, 0), light_blue),  # Change the background color to light blue
    ]))

    content.append(table)
    content.append(Spacer(1, 24))  # Increase the height of the spacer

    # Total amount
    total_amount = Paragraph(f'<para align="right">Amount: ${amount}</para>', getSampleStyleSheet()["Normal"])
    content.append(total_amount)
    content.append(Spacer(1, 24))

    # Thank you message
    thank_you = Paragraph("Thank you for using AnalyticX.", getSampleStyleSheet()["Normal"])
    content.append(thank_you)

    doc.build(content)
    buffer.seek(0)
    return buffer