import time
from selenium.webdriver.common.by import By
import re
import langid
import itertools
import random
import threading
import queue
from scrapy.crawler import CrawlerProcess
from billiard import Process, Queue


def find_name(url):
    print('reached find_name')
    if 'www.' in url:
        match = re.search(r'\.(.*?)\.', url)
        return match.group(1)
    else:
        match = re.search(r'/(?P<content>[^/]+)\.', url)
        return match.group('content')


import threading
import queue
from scrapy.crawler import CrawlerProcess

def get_info_by_scrapy(MySpider, url):
    result_queue = queue.Queue()
    stop_event = threading.Event()

    class CustomSpider(MySpider):
        def closed(self, reason):
            if not stop_event.is_set():
                result_queue.put(self.text_list)

    def _run_spider(url):
        process = CrawlerProcess()
        process.crawl(CustomSpider, start_url=url)
        process.start()
        if not stop_event.is_set():
            result_queue.put(MySpider.text_list)

    spider_thread = threading.Thread(target=_run_spider, args=(url,))
    spider_thread.start()

    # Set a timeout for the Scrapy process
    timeout = 30
    spider_thread.join(timeout)

    # If the Scrapy process is still running, set the stop_event and join the thread
    if spider_thread.is_alive():
        print("Terminating Scrapy process due to timeout...")
        stop_event.set()
        spider_thread.join()

    # Retrieve the results from the result_queue
    try:
        text_list = result_queue.get(block=False)
    except queue.Empty:
        text_list = []
        print("No results were retrieved due to the timeout.")

    return text_list

def get_info_by_selenium(driver, result_queue, stop_event):
    print('reached get_info')
    try:
        home_page_info1 = driver.find_elements(By.TAG_NAME, 'p')
    except:
        pass
    home_page_info = []
    home_page_info.extend(home_page_info1)
    for tag in ['p', 'a', 'span','h1', 'h2', 'h3', 'li']:
        try:
            home_page_info.extend(driver.find_elements(By.TAG_NAME, tag))
        except:
            pass
    text_list = []
    for i in home_page_info:
        try:
            text_list.append(i.text)
        except:
            pass
    text_list = list(set(text_list))
    for index, x in enumerate(text_list):
        if "\n"  in x or "\\u" in x:
            info = x.replace('\n', ' ').replace('\\u', '')
            text_list.remove(x)
            text_list.insert(index, info)
    print(text_list)
    if stop_event.is_set():
        print("Terminating get info thread...")
        return None
    else:
        result_queue.put(text_list)




def find_info(input):
    telephone = []
    email = []
    chin_list = []
    eng_list = []
    duplicated_test_list = input.copy()
    try:
        for index, info in enumerate(input):
            if '@' and '.' in info:

                pattern = r'(?<=[:\s：])\S+@\S+\.\S+|^[^:\s：]+@\S+\.\S+$'
                match = re.search(pattern, info)

                if match:
                    output = match.group()
                    email.append(output)
                    duplicated_test_list.remove(info)
                else:
                    pass

            elif re.search(r"\d{8}", info):
                telephone.append(re.findall(r"\d{8}", info))
                duplicated_test_list.remove(info)

            elif re.search(r"\d{4}-\d{4}", info):
                telephone.append(re.findall(r"\d{4}-\d{4}", info))
                duplicated_test_list.remove(info)

            elif re.search(r"\d{4} \d{4}", info):
                telephone.append(re.findall(r"\d{4} \d{4}", info))
                duplicated_test_list.remove(info)


            elif re.search(rf'(?:\d{{4}}.*Ltd.*|Ltd.*\d{{4}})|\d{{4}}.*有限公司.*|有限公司.*\d{{4}}|\d{{4}}.*Limited.*|Limited.*\d{{4}}'
                    , info, flags=re.IGNORECASE):
                duplicated_test_list.remove(info)

            elif re.search(rf'(?:\d+\s+)? Road|Street|街|道|號|里', info, flags=re.IGNORECASE):
                duplicated_test_list.remove(info)

            elif re.search(r'^(?=.*\bcookies\b)(?=.*\bwebsite\b)(?=.*\bexperience\b)', info, flags=re.IGNORECASE):
                duplicated_test_list.remove(info)

            elif re.search(r"www\.[a-zA-Z0-9\-\.]*\.(com)|WordPress|free|working hour|辦公時間|優惠|newsletter|all rights reserved|suscribe", info, flags=re.IGNORECASE):
                duplicated_test_list.remove(info)

            elif len(info) < 8:
                duplicated_test_list.remove(info)

            else:
                pass

        for x in duplicated_test_list:
            lang, _ = langid.classify(x)
            if lang == 'zh':
                chin_list.append(x)
            else:
                eng_list.append(x)

        email = list(set(email))
        telephone = list(itertools.chain(*telephone))
        telephone = list(set(telephone))

        return email, telephone, chin_list, eng_list

    except:
        return email, telephone, chin_list, eng_list




# all clicking pages are not needed
def helper_func_contact_page(contact_pages_texts,contact_pages_elements, driver, contact_us_keyword):
    for contact_pages_text, contact_pages_element in zip(contact_pages_texts, contact_pages_elements):
        result_queue = queue.Queue()
        stop_event = threading.Event()
        text_data = contact_pages_text
        helper_func_click(driver, contact_pages_element)
        if contact_us_keyword in text_data:
            break_outer_loop = True
            print('Sucessful clicking contact page:' + contact_us_keyword)
            time.sleep(2)
            t = threading.Thread(target=get_info_by_selenium, name="get_info", args=(driver, result_queue, stop_event))
            t.start()
            t.join(10)
            if t.is_alive():
                print("terminate contact page")
                stop_event.set()
                t.join()
                return [], []
            else:
                text_list = result_queue.get()
                print('got info from contact page')
                telephone, email, chin_list, eng_list = find_info(text_list)
                return telephone, email



def click_contact_page(contact_us, driver):
    global break_outer_loop
    break_outer_loop = False
    for index, contact_us_keyword in enumerate(contact_us):
        print('contact_us_keyword: ' + contact_us_keyword)
        contact_pages_elements = driver.find_elements(By.XPATH, f"//*[contains(text(), '{contact_us_keyword}')]")

        if contact_pages_elements != []:
            contact_pages_texts = [contact_pages_element.text for contact_pages_element in contact_pages_elements]
            print(contact_pages_texts)
            if all([elem.isspace() or elem == '' for elem in contact_pages_texts]):
                pass
            else:
                print('contact page tag')
                contact_pages_elements, contact_pages_texts = zip(
                    *[(e, t) for e, t in zip(contact_pages_elements, contact_pages_texts) if not (t.isspace() or t == '')])
                print(contact_pages_texts)
                telephone, email = helper_func_contact_page(contact_pages_texts, contact_pages_elements, driver,
                                                         contact_us_keyword)
                return telephone, email

        elif index != -1:
            print('contact page a tag')
            a_tag_contact_pages = driver.find_elements(By.XPATH, f"//a[contains(@href, '{contact_us_keyword}')]")
            a_tag_contact_pages_texts = [text.get_attribute('href') for text in a_tag_contact_pages]
            print(a_tag_contact_pages_texts)

            if a_tag_contact_pages != []:
                telephone, email = helper_func_contact_page(a_tag_contact_pages_texts, a_tag_contact_pages, driver,
                                                            contact_us_keyword)
                return telephone, email

        if break_outer_loop:
            break



def helper_func_click(driver, element):
    try:
        driver.execute_script("return arguments[0].click();", element)
        driver.execute_script("return arguments[0].click();", element)
    except:
        pass
    time.sleep(2)


def helper_func_about_page(about_page_texts,about_pages_elements, driver, about_us_keyword):
    for about_page_text, about_pages_element in zip(about_page_texts, about_pages_elements):
        result_queue = queue.Queue()
        stop_event = threading.Event()
        text_data = about_page_text
        helper_func_click(driver, about_pages_element)
        if about_us_keyword in text_data:
            break_outer_loop = True
            print('Sucessfully clicked keyword:' + about_us_keyword)
            time.sleep(2)
            t = threading.Thread(target=get_info_by_selenium, name="get_info", args=(driver, result_queue, stop_event))
            t.start()
            t.join(10)
            if t.is_alive():
                print("terminate about page")
                stop_event.set()
                t.join()
                return []# Wait for the thread to terminate
            else:
                text_list = result_queue.get()
                print(text_list)
                print('got info from about page')
                return text_list



def click_about_page(about_us, driver):
    print('enetred about function')
    global break_outer_loop
    break_outer_loop = False
    for index, about_us_keyword in enumerate(about_us):
        print('about_us_keyword: ' + about_us_keyword)
        about_pages_elements = driver.find_elements(By.XPATH, f"//*[contains(text(), '{about_us_keyword}')]")
        if about_pages_elements != []:
            about_page_texts = [about_pages_element.text for about_pages_element in about_pages_elements]
            print(about_page_texts)
            if all([elem.isspace() or elem == '' for elem in about_page_texts]):
                pass
            else:
                print('about page tag')
                about_pages_elements, about_page_texts = zip(*[(e, t) for e, t in zip(about_pages_elements, about_page_texts) if not (t.isspace() or t == '')])
                print(about_page_texts)
                about_page_list = helper_func_about_page(about_page_texts, about_pages_elements, driver, about_us_keyword)
                return about_page_list


        elif index != -1:
            a_tag_about_pages = driver.find_elements(By.XPATH, f"//a[contains(@href, '{about_us_keyword}')]")
            a_tag_about_pages_texts = [text.get_attribute('href') for text in a_tag_about_pages]
            print(a_tag_about_pages_texts)

            if a_tag_about_pages != []:
                print('about page a tag')
                about_page_list = helper_func_about_page(a_tag_about_pages_texts, a_tag_about_pages, driver,
                                                         about_us_keyword)
                return about_page_list
        if break_outer_loop:
            break
        if index == -1:
            print('Fail to click about page245')
            return []




def combine_data(input1, input2):
    content1 = []
    content2 = []
    chin_num = len(input1)
    eng_num = len(input2)
    unsplit_info1 = " ".join(input1)
    unsplit_info2 = " ".join(input2)
    content1.append(unsplit_info1)
    content2.append(unsplit_info2)
    all_content1 = ".".join(content1)
    all_content2 = ".".join(content2)
    all_content1 = re.sub(r'[^\w\s]', '.', all_content1)
    all_content2 = re.sub(r'[^\w\s]', '.', all_content2)
    return chin_num, eng_num, all_content1, all_content2



