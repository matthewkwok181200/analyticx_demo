from selenium.webdriver.common.keys import Keys
import re
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver


def turn_google_page(keyword, pattern, driver, times, result):

    ignore_list = ['google', 'amazon', 'facebook', 'instagram', 'openai']

    print('start' + str(times))


    driver.get('https://www.google.com')

    search_bar = driver.find_element("id", "APjFqb")

    search_bar.send_keys(keyword)

    search_bar.send_keys(Keys.ENTER)

    try:

        ending = driver.find_element(By.XPATH, '//*[@id="pnnext"]/span[2]')
        for i in range(1, 100):
            first_element = driver.find_element(By.CLASS_NAME, 'YyVfkd')
            following_element = first_element.find_element(By.XPATH, './following-sibling::*')
            following_element.click()
            try:
                for i in range(1, 10):
                    href = driver.find_element(By.XPATH, f'//*[@id="tads"]/div[{i}]/div/div/div/div[1]/a').get_attribute('href')

                    match = re.match(pattern, href)
                    if match.group(1) not in result:
                        if match.group(1) not in ignore_list:
                            result.append(match.group(1))
            except:
                pass

            try:
                for i in range(1, 10):
                    href = driver.find_element(By.XPATH, f'//*[@id="tadsb"]/div[{i}]/div/div/div/div[1]/a').get_attribute('href')
                    match = re.match(pattern, href)
                    if match.group(1) not in result:
                        if match.group(1) not in ignore_list:
                            result.append(match.group(1))
            except:
                pass

            try:
                rsos = driver.find_elements(By.CLASS_NAME, 'yuRUbf')

                for rso in rsos:
                    a_tag = rso.find_element(By.TAG_NAME, 'a')
                    href = a_tag.get_attribute('href')
                    match = re.match(pattern, href)
                    if match.group(1) not in result:
                        if match.group(1) not in ignore_list:
                            result.append(match.group(1))

            except:
                pass


    except:
        pass

    driver.get('https://www.google.com')














