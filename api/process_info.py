import spacy



def pos_tag_eng(eng_nlp, input):
    output = eng_nlp(input)
    list = []
    for token in output:
        if token.pos_ == "NOUN":
            list.append(token.text)
        elif token.pos_ == "VERB":
            list.append(token.text)
    eng_List = ' '.join(list)
    return eng_List

def pos_tag_chin(input):
    chin_pos = spacy.load("zh_core_web_sm", disable=["parser", "ner"])
    output = chin_pos(input)
    list = []
    for token in output:
        if token.pos_ == "NOUN":
            list.append(token.text)
        elif token.pos_ == "VERB":
            list.append(token.text)
    chin_List = ' '.join(list)
    return chin_List


def cosine_similarity_eng(eng_nlp,client, prospect):
    doc1 = eng_nlp(client)
    doc2 = eng_nlp(prospect)
    cos_sim = doc1.similarity(doc2)
    return cos_sim

def cosine_similarity_chin(chin_nlp,client, prospect):
    doc1 = chin_nlp(client)
    doc2 = chin_nlp(prospect)
    cos_sim = doc1.similarity(doc2)
    return cos_sim


def divide_space(input):
    output = input.split()
    output_num = len(output)
    return output_num, output

def same_word_comparison(client, prospect, prospect_num):
    client = [client.lower() for client in client]
    prospect = [prospect.lower() for prospect in prospect]
    count = 0
    for word in client:
        if word in prospect:
            count += 1
    similarity_score = round(count / prospect_num,3 )
    return similarity_score








