import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import random
import string
import os

def generate_verification_code(length=6):
    """Generate a random verification code."""
    letters = string.ascii_lowercase + string.digits
    return ''.join(random.choice(letters) for _ in range(length))

def send_notification_email(email_input, body, subject, print_message):
    sender = 'general@analyticx.online'
    # Create a multipart message object
    msg = MIMEMultipart()
    # Set the message subject, from, and to fields
    msg['Subject'] = subject #'AnalyticX notification - Report is ready'
    msg['From'] = sender
    print(email_input)
    msg['To'] = email_input
    # Add the message body
    body = body #'This is a notifcation email. The report is ready, please head to my-orders in the website to downlaod your report'
    msg.attach(MIMEText(body, 'plain'))

    # Create a SMTP connection object
    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    server.login(sender, '0')
    print("logged in")
    # Send the email
    server.sendmail(sender, email_input, msg.as_string())
    print(print_message)

    # Close the SMTP connection
    server.quit()


def find_file_with_any_extension(directory, base_filename):
    # List all files in the directory
    all_files = os.listdir(directory)
    print(all_files)

    for index, filename in enumerate(all_files):
        # Check if the filename starts with the base filename
        if base_filename in filename:
            return str(directory + all_files[index])



def send_client_email(client_email, client_password, email_input, body, subject, order_id, print_message):
    sender = client_email
    # Create a multipart message object
    msg = MIMEMultipart()
    # Set the message subject, from, and to fields
    msg['Subject'] = subject #'AnalyticX notification - Report is ready'
    msg['From'] = sender
    print(email_input)
    msg['To'] = 'general@analyticx.online'
    msg['Bcc'] = ', '.join(email_input)
    # Add the message body
    body = body #'This is a notifcation email. The report is ready, please head to my-orders in the website to downlaod your report'
    msg.attach(MIMEText(body, 'plain'))
    directory = "static/pitch_img/"
    base_filename = f"pitch_img_{order_id}"
    filename = find_file_with_any_extension(directory, base_filename)
    print(filename)
    try:
        with open(filename, 'rb') as file:
            attachment = MIMEBase('application', 'octet-stream')
            attachment.set_payload(file.read())
            encoders.encode_base64(attachment)
            attachment.add_header('Content-Disposition', f'attachment; filename={os.path.basename(filename)}')
            msg.attach(attachment)
    except:
        print("no file found")
    # Create a SMTP connection object
    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    server.login(client_email, client_password)
    print("logged in")
    # Send the email
    server.sendmail(sender, email_input, msg.as_string())
    print(print_message)

    # Close the SMTP connection
    server.quit()

