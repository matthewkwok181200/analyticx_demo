from api.handle_url import find_name, get_info_by_scrapy, get_info_by_selenium, click_about_page, find_info, click_contact_page, combine_data
from api.process_info import pos_tag_chin, pos_tag_eng, cosine_similarity_chin, cosine_similarity_eng
import threading
import queue
from selenium.common.exceptions import TimeoutException



def api_scrape(MySpider, driver2, about_us,contact_us, url_len,url_list, client_chin, client_eng, chin_nlp, eng_nlp):
    report = []
    similarity_score = 0
    for url in url_list[0:url_len]:
        company_info = {}
        print(str(url) + ' is being processed')
        name = find_name(url)
        text_list = get_info_by_scrapy(MySpider, url)

        print('trying to switch into selenium')

        if not text_list == None or text_list != []:

            print(text_list)
            email, telephone, chin_list, eng_list = find_info(text_list)

            print('finished find info')

            if email == [] and telephone == []:
                try:
                    driver2.get(url)
                except:
                    email, telephone = [], []

                print(str(url) + ' switched to selenium in the middle')
                try:
                    about_page_list = click_about_page(about_us, driver2)
                except:
                    about_page_list = None
                if not about_page_list == None:
                    text_list.extend(about_page_list)
                if not text_list == None or text_list != []:
                    email, telephone, chin_list, eng_list = find_info(text_list)
                    if email == []:
                        try:
                            email, telephone = click_contact_page(contact_us, driver2)
                        except:
                            email, telephone = [], []

            else:
                print('scrapy has finished')


        else:
            try:
                driver2.get(url)
                print(str(url) + 'switched to selenium at begining')
                name = find_name(url)
                result_queue = queue.Queue()
                stop_event = threading.Event()
                t = threading.Thread(target=get_info_by_selenium, name="get_info",
                                     args=(driver2, result_queue, stop_event))
                t.start()
                t.join(20)
                if t.is_alive():
                    print('getting info from index page too long, destory thread')
                    stop_event.set()
                    t.join()
                    home_page_list = []
                else:
                    print('index page get info ended organically')
                    home_page_list = result_queue.get()
                    try:
                        about_page_list = click_about_page(about_us, driver2)
                    except:
                        about_page_list = None
                if not about_page_list == None:
                    home_page_list.extend(about_page_list)
                if not home_page_list == None or home_page_list != []:
                    email, telephone, chin_list, eng_list = find_info(home_page_list)
                    print('selenium has finished')
                    if email == []:
                        try:
                            email, telephone = click_contact_page(contact_us, driver2)
                            print('selenium has clicked contact page')
                        except:
                            email, telephone = [], []
                            print('selenium has not clicked contact page')
            except:
                email, telephone, chin_list, eng_list = [], [], [], []
                print('selenium has timed out')




        chin_num, eng_num, chin_content, eng_content = combine_data(chin_list, eng_list)

        if chin_num > eng_num:
            prospect_chin_pos = pos_tag_chin(chin_content)
            client_chin_pos = pos_tag_chin(client_chin)
            similarity_score = cosine_similarity_chin(chin_nlp, client_chin_pos, prospect_chin_pos)
            lang = 'chin'

        elif chin_num < eng_num:
            prospect_eng_pos = pos_tag_eng(eng_nlp, eng_content)
            client_eng_pos = pos_tag_eng(eng_nlp, client_eng)
            similarity_score = cosine_similarity_eng(eng_nlp, client_eng_pos, prospect_eng_pos)
            lang = 'eng'

        else:
            lang = 'chin'

        company_info['公司名稱'] = name
        company_info['公司email'] = email
        company_info['公司電話'] = telephone
        company_info['目標相似評分'] = similarity_score
        company_info['網址語言'] = lang
        report.append(company_info)


    return report


